# reading


## Basic
### Java & Jvm
#### [Core Java Volume I & Volume II](https://www.amazon.com/Core-Java-I--Fundamentals-9th/dp/0137081898)
#### [Head First Java](https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208)
#### [Java Concurrency in Practice](https://www.amazon.com/Java-Concurrency-Practice-Brian-Goetz/dp/0321349601)
#### [Programming Concurrency on the JVM: Mastering Synchronization, STM, and Actors](https://www.amazon.com/Programming-Concurrency-JVM-Mastering-Synchronization/dp/193435676X)
#### [Java Performance: The Definitive Guide](https://www.amazon.com/Java-Performance-Definitive-Scott-Oaks/dp/1449358454)

### Refactor & Design
#### [Clean Code](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
#### [Refactoring: Improving the Design of Existing Code](https://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672)
#### [Head First Design Patterns](https://www.amazon.com/Head-First-Design-Patterns-Freeman/dp/0596007124)
#### [The Design of Design: Essays from a Computer Scientist](https://www.amazon.com/Design-Essays-Computer-Scientist/dp/0201362988)
#### [Pattern-Oriented Software Architecture](https://www.amazon.com/s?field-keywords=Pattern-Oriented+Software+Architecture)
#### [Patterns of Enterprise Application Architecture](https://www.amazon.com/Patterns-Enterprise-Application-Architecture-Martin/dp/0321127420)
#### [Applying UML and Patterns : An Introduction to Object-Oriented Analysis and Design and Iterative Development](https://www.amazon.com/Applying-UML-Patterns-Introduction-Object-Oriented/dp/0131489062)

### Software management & Agile practice
#### [The Pragmatic Programmer: From Journeyman to Master](https://www.amazon.com/Pragmatic-Programmer-Journeyman-Master/dp/020161622X)
#### [The Mythical Man-Month: Essays on Software Engineering,Anniversary Edition](https://www.amazon.com/Mythical-Man-Month-Software-Engineering-Anniversary/dp/0201835959)
#### [Peopleware: Productive Projects and Teams](https://www.amazon.com/Peopleware-Productive-Projects-Teams-3rd/dp/0321934113)

### Linux internal
#### [The Linux Programming Interface: A Linux and UNIX System Programming Handbook](https://www.amazon.com/Linux-Programming-Interface-System-Handbook/dp/1593272200)
#### [Internetworking with TCP/IP](https://www.amazon.com/Internetworking-TCP-Vol-1-Principles-Architecture/dp/0130183806)
#### [Unix Network Programming](https://www.amazon.com/Unix-Network-Programming-Sockets-Networking/dp/0131411551)

### Computer science
#### [The Art of Computer Programming](https://www.amazon.com/Art-Computer-Programming-Vol-Fundamental/dp/0201896834)
#### [Introduction to Algorithms](https://www.amazon.com/Introduction-Algorithms-3rd-MIT-Press/dp/0262033844)

### Effective
#### [The Seven Habits of Highly Effective People](https://www.amazon.com/Habits-Highly-Effective-People-Powerful/dp/1451639619)
#### [Pomodoro Technique Illustrated](https://www.amazon.com/Pomodoro-Technique-Illustrated-Pragmatic-Life/dp/1934356506)

## Advanced
### Performance
#### [Systems Performance: Enterprise and the Cloud](https://www.amazon.com/Systems-Performance-Enterprise-Brendan-Gregg/dp/0133390098)
#### [High performance browser networking](https://www.amazon.com/High-Performance-Browser-Networking-performance/dp/1449344763)
#### [Computer Systems: A Programmer's Perspective](https://www.amazon.com/Computer-Systems-Programmers-Perspective-3rd/dp/013409266X)
### Distributed Related